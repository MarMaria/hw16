﻿// HW16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <ctime>

int NewN()
{
	int N;
	do
	{
		std::cout << "Print array dimension > 0\n";
		std::cin >> N;
		std::cout << '\n';
	} while (N <= 0);

	return N;
}

int main()
{
	unsigned int N = NewN();

	int **arr;
	arr = new int*[N];
	for (int i = 0; i < N; i++)
	{
		arr[i] = new int[N];
	}

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			arr[i][j] = i + j;
			std::cout << arr[i][j] << '\t';
		}
		std::cout << '\n';
	}
	std::cout << '\n';

	const time_t currT = time(0);
	tm local;
	localtime_s(&local, &currT);
	int day = local.tm_mday;

	std::cout << "Today is " << day << "th" << '\n';

	int row = 0;
	while (row != day % N)
	{
		row++;
	}

	int summ = 0;
	for (int j = 0; j < N; j++)
	{
		summ += arr[row][j];
	}

	std::cout << "Row num = " << row << '\t' << "Summ = " << summ << '\n';


	for (int i = 0; i < N; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
